<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Zup g</title>
	<link rel="stylesheet" href="css/style.css">
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>
<div class="content">
	<h1><u>CSS timeline styling (and jQuery)</u></h1>
	<h3><u>Dynamically rendered</u></h3>
	<p>The data will be in a collection:</p>
	<pre>
	$colletction = [
		0 => 'content',
		1 => 'content',
		2 => 'content',
		3 => 'content',
		4 => 'content',
		5 => 'content',
		6 => 'content',
		7 => 'content',
		...
	]
	</pre>
	<ul>
		<?php
		for($x = 1; $x <= 100; $x++){
			?>
		  <li <?php echo $x == 6 ? 'class="active"' : ''; ?>><?php echo $x ?></li>
		<?php
		}
		?>
		</ul>
</div>

<div class="content">
	<h3><u>Mockup and expected result 😃</u></h3>

	<p>jQuery will have to calculate the order of the dom tree.</p>
	<p>Below is an <b>mockup image</b>. Notice that the numbers are in order from right to left. They should always be in order, independent of screen size. So numbers are always in order even if the page is viewed on a desktop computer, or view on a device (iphone etc). Resizing your screen keeps the order.</p>
	<image src="images/mockup.png" style="height: 135px; width: 800px;">
</div>

</body>
</html>
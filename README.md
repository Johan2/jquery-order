# CSS timeline styling

This is an mock-up display of the expected result.

To run this example code. In your terminal:

	1. git clone [project]
	2. cd [project]
	3. Run a client php server: $ php -S 0.0.0.0:8881 index.php
	4. Open http://0.0.0.0:8881 in your browser

jQuery and BootstrapCSS is allowed to use.